#include <iostream>

unsigned long long factorial(int n)
{
    unsigned long long result = 1;
    for (int i = 2; i <= n; ++i)
    {
        result *= i;
    }
    return result;
}

unsigned long long combinatoria(int n, int p)
{
    return factorial(n) / (factorial(p) * factorial(n - p));
}

int main()
{
    int n, p;

    std::cout << "Ingrese el valor de n: ";
    std::cin >> n;

    std::cout << "Ingrese el valor de p: ";
    std::cin >> p;

    unsigned long long resultado = combinatoria(n, p);

    std::cout << "El resultado de C(" << n << ", " << p << ") es: " << resultado << std::endl;
    return 0;
}
